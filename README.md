# Pesquisas e estudos em Programação Orientada a Objetos
## Conteúdo
###### [/introducao] - Implementações iniciais em OO - <a href="https://gitlab.com/gepds/ed/tree/master/listas/listas-ligadas" target="_blank">Implementações de Listas Ligadas com Orientação a Objetos</a> - 
<a href="https://gitlab.com/gepds/ed/tree/master/matrizes" target="_blank">Matrizes com Java</a>
###### [/heranca] - Implementações do conceito de Herança
###### [/abstracao] - Implementações do conceito de Classe e Métodos Abstratos
###### [/interface] - Implementações do conceito de Interfaces de Programação
###### [/polimorfismo] - Implementações do conceito de Poliformismo
###### [/associacao] - Implementações do conceito de Associação
###### [/composicao] - Implementações do conceito de Composição
###### [/agregacao] - Implementações do conceito de Agregação
###### [/designpatterns] - Implementações de Padrões de Projeto
###### [/uml] - Conteúdos e diagramas sobre a Linguagem de Modelagem Unificada
## Projetos
###### [/JK] - Conteúdo dos projetos, pesquisas e trabalhos sobre Programação Orientada a Objetos realizados com os alunos da Faculdade Jk de Tecnologia