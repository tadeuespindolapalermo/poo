package br.com.poo.heranca;
/**
 * Testando as classes desenvolvidas com método principal (main)
 * @author George Mendonça
 * @date 16/11/2016
 */
public class AppMenu {

	public static void main(String[] args) {
		GeraMenu gm = new GeraMenu();
		System.out.println(gm.vertical());
		System.out.println(gm.horizontal());
	}
}
/**
 * RESULTADO: 
 * 		> Menu Vertical
 * 		> Menu Horizontal
 */