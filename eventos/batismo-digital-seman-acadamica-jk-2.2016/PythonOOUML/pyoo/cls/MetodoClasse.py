# encoding: utf-8
'''
Created on 11 de nov de 2016

@author: George Mendonça
'''
class MetodoClasse:
    def f(self):
        print('Massa!')
    
    # A função classmethod transforma f em uma espécie
    # de método estático da classe, mas difere do 
    # concieo de Java e C++
    f = classmethod(f)
    
if __name__ == '__main__':
    MetodoClasse.f()