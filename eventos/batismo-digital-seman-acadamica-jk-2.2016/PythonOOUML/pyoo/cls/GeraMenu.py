# encoding: utf-8
'''
Created on 12 de nov de 2016

@author: George Mendonça
'''
from pyoo.cls.Menu import Menu

class GeraMenu(Menu):
    
    def vertical(self):
#         return Menu.gera(self, '> Menu Vertical').getTipo()
        return Menu.gera(self, '> Menu Vertical').getTipo()
    
    def horizontal(self):
        return Menu.gera(self, '> Menu Horizontal').getTipo()
    
if __name__ == '__main__':
    menu = GeraMenu()
    print(menu.vertical())
    print(menu.horizontal())    