/**
 * Implementação de uma classe Conta Bancária simples
 * @author George Mendonça
 * Data: 13/10/2016
 * Atualização:
 * */
public class Conta implements IConta {
	private String titular;
	private int numero;
	private Double saldo;
	
	public Double sacar(Double valor) {
		return this.saldo = this.saldo-valor;
	}
	
	public Double depositar(Double valor) {
		return this.saldo = this.saldo+valor;
	}
	
	public String getTitular() {
		return this.titular;
	}
	
	public void setTitular(String t) {
		this.titular = t;
	}
	
	public int getNumero() {
		return this.numero;
	}
	
	public void setNumero(int n) {
		this.numero = n;
	}
	
	public Double getSaldo() {
		return this.saldo;
	}
	
	public void setSaldo(Double s) {
		this.saldo = s;
	}
	
	public void extrato(Conta c) {
		System.out.println("================= Extrato =================");
		System.out.println("Titular: "+c.getTitular());
		System.out.println("Número: "+c.getNumero());
		System.out.println("Saldo: "+c.getSaldo());
		System.out.println("===========================================");
	}
}
