/**
 * Implementação de uma classe Conta Bancária simples
 * @author George Mendonça
 * Data: 13/10/2016
 * Atualização:
 * */
interface IConta {	
	public Double sacar(Double valor);	
	public Double depositar(Double valor);	
	public void extrato(Conta c);
}
