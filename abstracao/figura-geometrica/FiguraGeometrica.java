/**
 * Classe Abastrata FiguraGeometrica
 * @author George Mendonça - georgehrem@gmail.com
 * Data: 20/10/2016
 * Atualização: 26/10/2016
 */
package br.com.poo.classeabstrata;

public abstract class FiguraGeometrica implements IFiguraGeometrica {

	/**
	 * Atributos concretos da classe abstrata FiguraGeometrica
	 */
	private String nome;
	private double perimetro;
	private double area;
	private double volume;
	
	/**
	 * Métodos abstratos da classe abstrata FiguraGeometrica
	 */
	public abstract double perimetro();
	public abstract double area();
	public abstract double volume();

	/**
	 * Métodos concretos implementados da interface IFiguraGeometrica
	 */
	@Override
	public String getNome() {
		return this.nome;
	}

	@Override
	public void setNome(String n) {
		this.nome = n;
	}
	
	@Override
	public double getPerimetro() {
		return this.perimetro;
	}

	@Override
	public void setPerimetro(double p) {
		this.perimetro = p;
	}

	@Override
	public double getArea() {
		return this.area;
	}

	@Override
	public void setArea(double ar) {
		this.area = ar;
	}

	@Override
	public double getVolume() {
		return this.volume;
	}

	@Override
	public void setVolume(double vm) {
		this.volume = vm;		
	}
}